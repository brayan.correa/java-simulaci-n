package numeros.pruebas;

import javax.swing.JOptionPane;

public class Prueba_Poker {

 
	public Prueba_Poker()
	{
		
	}
	
	public boolean getpoker(double nps[],int n,double alfa,int periodo)
	{
		int vector[];
                int vec_fobservada[];
                
		double vec_fesperadaI[],x02=0,xan1,pmano[]={0.30240,0.50400,0.10800,0.07200,0.00900,0.00450,0.00010}; //posibilidades de cada una de las manos de poker
		
                int N=periodo;
		vector=new int[periodo];
                
		vec_fobservada=new int[7]; //vector de la frecuencia observada
		vec_fesperadaI=new double[7]; //vector de la recuencia esperada
                
		for(int i=0; i<periodo; i++)
		{
			vector[i]=(int)(nps[i]*1000000000);
		}
                
		getfo(vector,vec_fobservada);
		for(int i=0;i<vec_fobservada.length;i++)
		{
			vec_fesperadaI[i]=pmano[i]*N;
		}
                
		rodar(vec_fobservada,vec_fesperadaI);
		for(int i=0;vec_fesperadaI[i]<vec_fesperadaI.length;i++)
		{
			x02=Math.pow(vec_fobservada[i]-vec_fesperadaI[i],2)/vec_fesperadaI[i];//el estadistico
		}
                //tiene 6 grados de livertada ya que tienen 7 categorias
                //Todos diferentes
                //Un par
                //dos pares
                //tercia
                //full
                //poker
                //quintilla
		xan1=Chi2.getchi2().ObtenerChi(alfa, n-1); // requiere el estadistico de la distribución del chi cuadrado
		if(x02<xan1){// si el estadistico X20 es < que el estadistico del Chi cuadrado
                        JOptionPane.showMessageDialog(null, "X02"+" "+x02+"\n"+"Chi"+" "+xan1+"\n"+"si el estadistico X20 es < que el estadistico del Chi cuadrado"+"\n"+"Proviene a una distribución uniforme");
                        return true;
                }
		else{
                       JOptionPane.showMessageDialog(null, "X02"+" "+x02+"\n"+"Chi"+" "+xan1+"\n"+"si el estadistico X20 es > que el estadistico del Chi cuadrado"+"\n"+" ");
                        return false;
                }
	}
        
        
        
	public void rodar(int a[],double fe[])
	{
		for(int i=1;i<fe.length;i++)
		{
			if(fe[i]<5)
			{
				for(int j=i-1;j>=0;j--)
				{
					if(fe[j]>0)
					{
						fe[j]+=fe[i];
						fe[i]=0;
						a[j]+=a[i];
						a[i]=0;
					}
						
				}
			}
		}
	}
        
	public void limpiar(int digitos[])
	{
		for(int i=0;i<digitos.length;i++)
			digitos[i]=0;
	}
        
	public void getfo(int vec[],int vec_fo[])
	{
		int max=0,max2=0,index=0,sapo=0;
		String aux="";
		int digitos[]=new int[10];
		limpiar(digitos);
		
		for(int i=0;i<vec.length;i++)
		{
			aux="";
			max=max2=0;
			aux+=vec[i];
			limpiar(digitos);
			if(aux.length()<5)
				digitos[0]=5-aux.length();
			for(int j=0;j<aux.length();j++)
			{
				index=aux.charAt(j)-'0';
				digitos[index]++;
				if(digitos[index]>max){
					max=digitos[index];
					sapo=index;
				}
			}
			if(max==4)
				vec_fo[5]++;
			else if(max==5)
				vec_fo[6]++;
			else if(max<=3)
			{
				for(int i1=0;i1<digitos.length;i1++)
				{
					if(digitos[i1]>max2 && i1!=sapo)
						max2=digitos[i1];						
				}
				if(max==3 && max2==1)
				{					
					vec_fo[3]++;
					
				}
				else if(max==3 && max2==2){
					vec_fo[4]++;
					
				}
				else if(max==2 && max2==2)
					vec_fo[2]++;
				else if(max==2 && max2==1)
					vec_fo[1]++;
				else if(max==1)
					vec_fo[0]++;
			}
			
			
		}
		
	}

}
