
package Vista;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import  numeros.pruebas.*;

public class JPanelPrueba_Est extends javax.swing.JPanel {
    ArrayList<Float> numeros ;
    
    public JPanelPrueba_Est() {
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jCheckBoxPrueba1 = new javax.swing.JCheckBox();
        jComboBoxPrueba1 = new javax.swing.JComboBox();
        jSeparator2 = new javax.swing.JSeparator();
        jButtonRealizarPrueba = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();

        jPanel1.setMinimumSize(new java.awt.Dimension(600, 400));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel2.setText("Seleccionar Prueba");

        jCheckBoxPrueba1.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jCheckBoxPrueba1.setSelected(true);
        jCheckBoxPrueba1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jCheckBoxPrueba1StateChanged(evt);
            }
        });
        jCheckBoxPrueba1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPrueba1ActionPerformed(evt);
            }
        });

        jComboBoxPrueba1.setMaximumRowCount(5);
        jComboBoxPrueba1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Promedios", "Frecuencia", "Series", "Poker", "Kolmogorov" }));
        jComboBoxPrueba1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxPrueba1ActionPerformed(evt);
            }
        });

        jSeparator2.setMinimumSize(new java.awt.Dimension(600, 400));

        jButtonRealizarPrueba.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jButtonRealizarPrueba.setText("Evaluar");
        jButtonRealizarPrueba.setToolTipText("Iniciar pruebas");
        jButtonRealizarPrueba.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRealizarPruebaActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jList1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(38, 38, 38)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(44, 44, 44)
                            .addComponent(jLabel2))
                        .addComponent(jButtonRealizarPrueba, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jComboBoxPrueba1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jCheckBoxPrueba1)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jButtonRealizarPrueba, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jCheckBoxPrueba1)
                        .addGap(306, 306, 306))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBoxPrueba1)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jCheckBoxPrueba1, jComboBoxPrueba1});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(285, 285, 285)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(333, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(86, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRealizarPruebaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRealizarPruebaActionPerformed

        int n = numeros.size();
        double vector[]=new double[n];
        for(int i=0;i<n;i++){
            vector[i]= numeros.get(i);
        }
        //Prueba
        if(jCheckBoxPrueba1.isSelected()){
            boolean W=false;
            switch(jComboBoxPrueba1.getSelectedIndex()){
                case 0:{
                    Prueba_Promedios p = new Prueba_Promedios();
                    W = p.getpromedios(vector, 1.96, n);
                    break;
                }
                case 1:{
                    Prueba_Frecuencia f= new Prueba_Frecuencia();
                    W =f.getfrecuencias(vector, 6, 0.05, n);
                    break;
                }
                case 2:{
                    Prueba_Series s = new Prueba_Series();
                    W= s.getseries(vector, 5, 0.05, n);
                    break;
                }
                case 3:{
                    Prueba_Poker pk= new Prueba_Poker();
                    W= pk.getpoker(vector, 5, 0.05, n);
                    break;
                }
                case 4:{
                     Prueba_Kolmogorov p = new Prueba_Kolmogorov();
                     W = p.getkolmogorov(vector, 1.96, n);
                    break;
                }
            }
            if(W==true){
                JOptionPane.showMessageDialog(null, "Los Numeros Pseudoaleatorios provienen de una " +
                    "\ndistribucion uniforme, por lo tanto no puede ser rechazados..");

            }
            else{
                JOptionPane.showMessageDialog(null, "Los Numeros Pseudoaleatorios NO provienen de una " +
                    "\ndistribucion uniforme, por lo tanto son rechazados..");

            }
        }
    }//GEN-LAST:event_jButtonRealizarPruebaActionPerformed

    private void jComboBoxPrueba1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxPrueba1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxPrueba1ActionPerformed

    private void jCheckBoxPrueba1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPrueba1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxPrueba1ActionPerformed

    private void jCheckBoxPrueba1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBoxPrueba1StateChanged
        if(jCheckBoxPrueba1.isSelected()){
            jComboBoxPrueba1.setEnabled(true);
        }else{
            jComboBoxPrueba1.setEnabled(false);

        }
    }//GEN-LAST:event_jCheckBoxPrueba1StateChanged

    public void llenarLista(){
        if(!numeros.isEmpty()){
            DefaultListModel lm = new DefaultListModel();

            jList1.setModel(lm);
            for(int i=0;i<numeros.size();i++){
                lm.addElement((i+1)+": "+numeros.get(i));
            }
            jList1.setModel(lm);
        }else{
            System.out.println("Error");
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRealizarPrueba;
    private javax.swing.JCheckBox jCheckBoxPrueba1;
    private javax.swing.JComboBox jComboBoxPrueba1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    // End of variables declaration//GEN-END:variables

    void setNumeros(ArrayList<Float> numeros) {
        this.numeros = numeros;
    }

}
