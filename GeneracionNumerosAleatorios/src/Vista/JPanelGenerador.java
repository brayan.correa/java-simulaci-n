/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Generador.java
 *
 * Created on 31/08/2011, 10:37:56 AM
 */

/*
 * Autores:
 * Luis Alfonso Velez Santos
 * Carlos Yesid Hernandez Herrera
 * Jose Esteban Betin Diaz
 */

package Vista;

import GenaracionNumeros.NumerosRectangulares;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import auxiliares.*;
import javax.swing.DefaultListModel;


public class JPanelGenerador extends javax.swing.JPanel {
    private ArrayList<Float> numeros;
    private NumerosRectangulares gna=new NumerosRectangulares();
    public JPanelGenerador() {
        initComponents();

        jComboBox1.setSelectedIndex(0);
        seleccionMetodo();

    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabelM = new javax.swing.JLabel();
        jTextFieldM = new javax.swing.JTextField();
        jLabelX0 = new javax.swing.JLabel();
        jTextFieldA = new javax.swing.JTextField();
        jTextFieldX0 = new javax.swing.JTextField();
        jLabelA = new javax.swing.JLabel();
        jTextFieldC = new javax.swing.JTextField();
        jLabelC = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jLabel11 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabelPeriodo = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jButtonLimpiar = new javax.swing.JButton();
        jButtonGenerar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(600, 500));
        setPreferredSize(new java.awt.Dimension(600, 500));

        jPanel1.setMinimumSize(new java.awt.Dimension(600, 400));
        jPanel1.setPreferredSize(new java.awt.Dimension(700, 500));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 3, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Números Aleatorios");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel2.setText("Seleccionar");

        jComboBox1.setMaximumRowCount(6);
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "\" \"", "Congruencial Mixto" }));
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        jLabelM.setText("Modulo M");

        jTextFieldM.setDocument(new LimitadorSoloNumerosNumMaxCaracteres(jTextFieldM, 9));
        jTextFieldM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldMActionPerformed(evt);
            }
        });

        jLabelX0.setText("Semilla Xo");

        jTextFieldA.setDocument(new LimitadorSoloNumerosNumMaxCaracteres(jTextFieldA, 9));
        jTextFieldA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldAActionPerformed(evt);
            }
        });

        jTextFieldX0.setDocument(new LimitadorSoloNumerosNumMaxCaracteres(jTextFieldX0, 9));
        jTextFieldX0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldX0ActionPerformed(evt);
            }
        });

        jLabelA.setText("Multiplicador A");

        jTextFieldC.setDocument(new LimitadorSoloNumerosNumMaxCaracteres(jTextFieldC, 9));
        jTextFieldC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCActionPerformed(evt);
            }
        });

        jLabelC.setText("Constante C");

        jScrollPane1.setViewportView(jList1);

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel11.setText("Numeros Generados");

        jLabelPeriodo.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabelPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 659, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabelPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jCheckBox1.setText("Validar");

        jButtonLimpiar.setText("LIMPIAR");
        jButtonLimpiar.setToolTipText("Limpiar");
        jButtonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLimpiarActionPerformed(evt);
            }
        });

        jButtonGenerar.setText("GENERAR");
        jButtonGenerar.setToolTipText("Generar");
        jButtonGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGenerarActionPerformed(evt);
            }
        });

        jButton1.setText("VOLADO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 537, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jCheckBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabelC)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldC, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabelA)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldA, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelM, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabelX0)
                                            .addGap(127, 127, 127)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldX0, javax.swing.GroupLayout.DEFAULT_SIZE, 63, Short.MAX_VALUE)
                                        .addComponent(jTextFieldM, javax.swing.GroupLayout.DEFAULT_SIZE, 63, Short.MAX_VALUE)))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(49, 49, 49)
                                .addComponent(jButtonGenerar, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(121, 121, 121)
                                .addComponent(jButton1)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jButtonLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane1, 0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(150, 150, 150))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jTextFieldA, jTextFieldC, jTextFieldM, jTextFieldX0});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jCheckBox1))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonLimpiar)
                        .addGap(14, 14, 14))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(jButton1)
                        .addGap(65, 65, 65)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelX0)
                            .addComponent(jTextFieldX0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelM)
                            .addComponent(jTextFieldM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelA)
                            .addComponent(jTextFieldA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelC))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonGenerar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGenerarActionPerformed
        generarNumeros();
    }//GEN-LAST:event_jButtonGenerarActionPerformed

    private void jButtonLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLimpiarActionPerformed
        jTextFieldA.setText("");
        jTextFieldC.setText("");
        jTextFieldM.setText("");
        jTextFieldX0.setText("");

        DefaultListModel lm = new DefaultListModel();
        jList1.setModel(lm);
        numeros.clear();
    }//GEN-LAST:event_jButtonLimpiarActionPerformed

    private void jTextFieldCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCActionPerformed

    private void jTextFieldX0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldX0ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldX0ActionPerformed

    private void jTextFieldAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldAActionPerformed

    private void jTextFieldMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldMActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldMActionPerformed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        seleccionMetodo();
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        volados();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void seleccionMetodo(){
        // C.Mixto
        switch(jComboBox1.getSelectedIndex()){

            
              
            case 1://C.Mixto
                jCheckBox1.setEnabled(true);
                jTextFieldC.setVisible(true);
                jTextFieldM.setVisible(true);
                jTextFieldX0.setVisible(true);
                jTextFieldA.setVisible(true);
            
                jLabelA.setVisible(true);
                jLabelC.setVisible(true);
                jLabelM.setVisible(true);
                jLabelX0.setVisible(true);
                
            break;
                
                
                
           

        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonGenerar;
    private javax.swing.JButton jButtonLimpiar;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelA;
    private javax.swing.JLabel jLabelC;
    private javax.swing.JLabel jLabelM;
    private javax.swing.JLabel jLabelPeriodo;
    private javax.swing.JLabel jLabelX0;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTextFieldA;
    private javax.swing.JTextField jTextFieldC;
    private javax.swing.JTextField jTextFieldM;
    private javax.swing.JTextField jTextFieldX0;
    // End of variables declaration//GEN-END:variables

    void setNumeros(ArrayList<Float> numeros) {
        this.numeros = numeros;
    }

    private void generarNumeros() {
            switch(jComboBox1.getSelectedIndex()){
               
                case 1://C.Mixto
                    if(!numeros.isEmpty()){
                       numeros.clear();
//                        numeros = new ArrayList<Float>();
                    }
                    if(!(jTextFieldM.getText().equals("")||jTextFieldA.getText().equals("")||
                            jTextFieldX0.getText().equals("")||jTextFieldC.getText().equals(""))){
                        if(jCheckBox1.isSelected()){
                            numeros.addAll(gna.congruencialMixtoValidado(Auxiliares.convertirStringAEntero(jTextFieldM),
                                    Auxiliares.convertirStringAEntero(jTextFieldC), Auxiliares.convertirStringAEntero(jTextFieldA),Auxiliares.convertirStringAEntero(jTextFieldX0)));
                            jLabelPeriodo.setText("Periodo: "+numeros.size());
                            llenarLista();
                            jLabelPeriodo.setText("Periodo: "+numeros.size());
                        }else{
                            numeros.addAll(gna.congruencialMixto(Auxiliares.convertirStringAEntero(jTextFieldM),
                                    Auxiliares.convertirStringAEntero(jTextFieldC), Auxiliares.convertirStringAEntero(jTextFieldA),Auxiliares.convertirStringAEntero(jTextFieldX0)));
                            jLabelPeriodo.setText("Periodo: "+numeros.size());
                            llenarLista();
                            jLabelPeriodo.setText("Periodo: "+numeros.size());
                        }
                    }else{
                        JOptionPane.showMessageDialog(this, "Hay Campos vacios", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                    
                    
                   
               
            }
    }
    
    private void volados (){
        int cantidadDespuesVolado;
        int siguienteCorrida;
        
        int meta = Integer.parseInt(JOptionPane.showInputDialog("Ingrece meta alcanza"));
        int numeroC = Integer.parseInt(JOptionPane.showInputDialog("Ingrece número de corridas"));
        int cantidadAntesVolado = Integer.parseInt(JOptionPane.showInputDialog("Ingrece cantidad que se tiene en dolares"));
        int apuestaInicial = Integer.parseInt(JOptionPane.showInputDialog("Ingrese apuesta inicial"));
        
        System.out.println("numero de corridas:"+" "+numeroC);
        System.out.println("cantidad total inicial:"+" "+cantidadAntesVolado);
        System.out.println("cantidad de apuesta inicial:"+" "+apuestaInicial);
        
        for (int i = 0; i < numeroC; i++) {
            
        
            if (numeros.size()>0.5) {//arregal la condición
                System.out.println("Gano");
                cantidadDespuesVolado = apuestaInicial + cantidadAntesVolado;
                System.out.println("Cantidad depues del volado"+" "+cantidadDespuesVolado);
                    
                    if (cantidadDespuesVolado == meta) {
                        System.out.println("Cumplio la meta");
                        break;
                    }else{
                        siguienteCorrida = cantidadDespuesVolado+cantidadAntesVolado;
                        System.out.println("Cantidad que se tiene en la siguiente corrida"+""+siguienteCorrida);
                    }
                
            }else{
                System.out.println("Perdio");
            }
        }
    }

    
    
    public void llenarLista(){
        if(!numeros.isEmpty()){
            DefaultListModel lm = new DefaultListModel();
            jList1.setModel(lm);
            for(int i=0;i<numeros.size();i++){
                lm.addElement((i+1)+": "+numeros.get(i));
            }
            jList1.setModel(lm);
        }else{
            System.out.println("Error");
        }
    }

}
