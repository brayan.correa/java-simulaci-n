
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableMode1;
import componentes.CompVentana;


public class CongruencialMixto extends CompVentana
{
        private JTable                  tabla;
        private DefaultTableMode1       modelo;
        private JScrollPane             desplazamiento;
        private int[]                   arreglo ={0,1,2,3,4,5,6,7};
        private int                     columna1, columna3, columna2, columna5;
        private double                  columna4;
    
    public CongruencialMixto()
    {
        super("Congruencial Mixto", false, 0,0,200,100);
    }
    protected void crearContenido()
    {
        String[] columnas={"N","Xn","5Xn+7","(5Xn+7/8)","Xn+1"};
        tabla = new JTable();
        modelo = new DefaultTableMode1();
        desplazamiento = new JScrollPane(tabla);
        modelo.setColumnIdentfiers(columnas);
        desplazamiento.setHorizontalScrollBarPolicy(30);
        desplazamiento.setVerticalScrollBarPolicy(20);
        tabla.setAutoResizeMode(4);
        tabla.setFillsViewportHeight(true);
        pPie.setBackground(Color.blue);
        getContentPane().add(desplazamiento, "Center");
        pack();
        setTitle("Simulación");
        setDefaultCloseOperation(2);
        getContentPane().setLayout(new BorderLayout());
        setResizble(false);
        setVisible(true);
    }
    public void mixto ()
    {
        System.out.println("n---------nx-------5xn+7---------5xn+7/8--------xn+1-------"+"/n");
        columna2 = 4;
        Object[] fila 0 = new Object[5];
        for(int i=0; i<arreglo.length; i++)
        {
            columna1 = arreglo[i];
            columna3 = (5*columna2)+7;
            columna4 = (columna3/8);
            columna5 = columna3%8;
            System.out.println(columna1+"          "+columna2+"          "+columna3+"        "+columna4+"        "+columna5);
            System.out.println("------------------------------------------------------------------------------------------");
            modelo.addRow(fila);
            columna2=columna5;
        }
        tabla.setModel(modelo);
    } 
    public void actionPerformed(ActionEvent pE)
    {

    }
    public static void main(String[] args)
    {
        new CongruencialMixto().mixto();
    }
}