

package congruencialmultiplicativo;

import java.util.Scanner;


public class CongruencialMultiplicativo {

    public static void main(String[] args) {
        
        // Entradas
        int semilla_Xo = 0;
        int multiplicador_a = 0;
        int modulo_m = 0;
        
        int d = 0;
        int fases;
        
        System.out.println("-------------Sistema Decimal---------------");
        Scanner entrada = new Scanner(System.in); 
        
        // para ingresar valor de d que pertenese al modulo
    
        System.out.println("Ingrese valor de d para m");
        d = entrada.nextInt();
            if (d % 2 != 0 && d % 5 == 0) {

                System.out.println("La semilla no debe ser entero impar divisible para 2 o para 5 ");

            }else{
                    // para generar el multiplicador a
                    int t ;
                    int p ;

                    System.out.print("Para generar multiplicador a --->ingrese valor de t ...entero cualquiera ");
                    t = entrada.nextInt();

                    System.out.print("Para generar multiplicador a -->ingrese valor de p ...seleccione entre 3-11-13-19-21-27-29-37-53-59-61-67-69-77-83-91 ");
                    p = entrada.nextInt();

                        if (p == 3 || p == 11 || p == 13 || p == 19 || p == 21 || p == 27 || p == 29 || p ==37 || p == 53 || p == 59 || p == 61 || p == 67 || p == 69 || p ==77 || p == 83 || p == 91) {

                            // para generar el multiplicador a


                            multiplicador_a = (200 * t) + p;
                            System.out.println("MUltiplicador"+" "+ multiplicador_a);
                            System.out.println("Valor de p" + " "+p);
                            fases = (int) Math.pow(2, d-1) * (int) Math.pow(5, d-1);
                            System.out.println("valor del fase"+" "+fases);


                            // para generar la modulo m

                            modulo_m= (int) Math.pow(10, d);
                            System.out.print("Valor del modulo es " + modulo_m+"\n");


                            // para generar la semilla Xo

                            semilla_Xo=nPrimo_Cercano(modulo_m);
                            System.out.print("Valor de la semilla " + modulo_m);


                            //Operaciones
                            int i, numero;  
                            double numero2; 

                                for (i=0; i< fases; i++){
                                   numero = (multiplicador_a*semilla_Xo) % modulo_m;   
                                   numero2 = (double)numero / (double)(modulo_m-1);
                                   System.out.printf("%d. %d (%.4f)\n", i ,numero ,numero2 ); 
                                   semilla_Xo = numero;  
                                }

                        }else{
                            System.out.println("Este parametro no esta permitido"+"\n"+"ingrese cualquiera de estos números 3-11-13-19-21-27-29-37-53-59-61-67-69-77-83-91" + p);  

                        }
            }

        
    }
    
    
    
    public static int nPrimo_Cercano(int num) {
        int cont = 2;
        boolean n_primo = true;

        while ((n_primo) && (cont != num)) {
            if (num % cont == 0) {
                n_primo = false;
            }
            cont++;
        }
        if (n_primo) {
            return num;
        } else {
            return nPrimo_Cercano(num - 1);
        }
    }
    
}




