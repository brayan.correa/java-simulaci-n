
import javax.swing.JOptionPane;
public class Juego_Volados {
    public static void main(String[] args) {


        int corridas= Integer.parseInt(JOptionPane.showInputDialog("Ingrese número de coridas"));
        int dineroInicial = Integer.parseInt(JOptionPane.showInputDialog("Ingrece cantidad que se tiene en dolares"));
        int apuestaInicail = Integer.parseInt(JOptionPane.showInputDialog("Ingrese apuesta inicial"));
        
        boolean bmeta=true;
        while (bmeta) {            
            int objetivo = 50;
            if (objetivo>dineroInicial) {
                bmeta= false;
            }else{
                JOptionPane.showMessageDialog(null, "El dinero de Meta debe ser mayor que el dinero inical");
            }
        }
        
        
//doblar la apuesta cada vez que se pierde. Por ejemplo, si se apuesta $X y se pierde, entonces se apuesta $2X; si en esta
//ocasión se vuelve a perder, entonces, se apuesta $4X y así sucesivamente. 
//Sin embargo si al seguir esta política sucede que la apuesta es mayor que la cantidad de que se dispone,
//entonces, se apuesta lo que se tiene disponible. Por el contrario cada vez que se gane, la
//apuesta será de $X. Si la cantidad inicial disponible es de $30, la apuesta es de $10, la
//ganancia es igual a la cantidad apostada. La probabilidad de ganar en un volado es 0.5
//De total de dinero se realiza apuesta para llegar a una meta


//1. Se genera numero aleatorio entre 0 y 1
//2. Si numero aleatorio < 0.5 entonces
//3. gano total de dinero = total de dinero + apuesta si total de dinero = meta fin de esa corrida
//caso contrario
//4. perdio total de dinero = total de dinero – apuesta y apuesta = apuesta* 2 si
//   apuesta > total de dinero entonces apuesta = total de dinero



//Como todo consiste en un juego de apuestas con reglas definidas basamos nuestro modelo
//matemático en la metodología de apuestas que no es mas que las mismas reglas definidas
//en el párrafo anterior y consideramos que nuestro modelo matemático requiere de cierto
//grado de análisis para su implementación. Una vez terminado el juego se puede ver cual fue
//la probabilidad de ganar que es de:
//Probabilidad = cantidad de veces que se gano / cantidad de corridas
        
        int c = corridas;
        String aux="";
        int gano = 0, perdio = 0;
        int cantidadAntes = dineroInicial;
        int apuestaAntes = 0;
        System.out.println("| Corridas      |  Dinero Antes |    Apuesta\t|\tNumeroAleatorio\t\t|\tGano?\t|Cantidad Despues|\tMeta\t|+--------------+");
        for (int i = 0; i < corridas; i++) {
            boolean b = true;
            int dinero = dineroInicial;
            int apuesta = apuestaInicail;
            int meta = 70; // la meta
            
            while (b) {
            //for (int j = 1; j <  corridas; j++) {
                double random = Math.random() * 1;
                cantidadAntes= dinero;
                apuestaAntes=apuesta;
                if (random < 0.5) {
                    dinero = dinero + apuesta;
                    aux = "Ganó";
                    apuesta=apuestaInicail;
                } else {
                    dinero = dinero - apuesta;
                    aux = "Perdio";
                    if (dinero >= (2 * apuesta)) {
                        apuesta = 2 * apuesta;
                    } else {
                        apuesta = dinero;
                    }

                }
                if (dinero >= meta) {
                    b = false;
                    gano++;
                    c--;
                    System.out.println("|    "+(i+1)+"  \t|\t"+cantidadAntes+"\t|\t"+apuestaAntes+"\t|\t"+random+"\t|\t"+aux+"\t|\t"+dinero+"  \t |\tSi\t|");

                } else {
                    if (dinero <= 0) {
                        b = false;
                        perdio++;
                        c--;
                        System.out.println("|    "+(i+1)+"  \t|\t"+cantidadAntes+"\t|\t"+apuestaAntes+"\t|\t"+random+"\t|\t"+aux+"\t|\t"+dinero+"  \t |\tNo\t|");
                    }else{
                        System.out.println("|    "+(i+1)+"  \t|\t"+cantidadAntes+"\t|\t"+apuestaAntes+"\t|\t"+random+"\t|\t"+aux+"\t|\t"+dinero+"  \t |\t-\t|");
                    }
                }

            }

        }
        //System.out.println("Ganadas: " + gano + "\n Perdio: " + perdio);
    }

}
